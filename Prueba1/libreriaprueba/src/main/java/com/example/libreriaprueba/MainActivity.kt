package com.example.libreriaprueba

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*;

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button.setOnClickListener{
            val nro1 = ed1.text.toString().toInt()
            val nro2 = ed2.text.toString().toInt()
            val suma = nro1 + nro2
            tv1.text = "Resultado: ${suma.toString()}"
        }
    }
}
